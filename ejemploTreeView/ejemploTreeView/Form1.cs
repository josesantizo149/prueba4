﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace ejemploTreeView
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TreeNode año = new TreeNode("2017");

            TreeNode mes = new TreeNode("mayo");
            mes.Name = "algo";

            TreeNode doc = new TreeNode("doc1");

            doc.Tag = @"C:\Users\Usuario Dell\Desktop\PPlenguajes.txt"; 

            //mes.Nodes.Add(@"C:\Users\Usuario Dell\Desktop\prubonadomd.txt"); 


            mes.Nodes.Add(doc); 
            año.Nodes.Add(mes);
             
            treeView1.Nodes.Add(año);

            
         }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

            richTextBox1.Text = e.Node.Name;
            
        //    string archivo = e.Node.Tag.ToString(); 

        //    using (StreamReader sr = new StreamReader(archivo, Encoding.Default))
        //    {
        //        richTextBox1.Text = sr.ReadToEnd(); 
        //    }
        //}

        //private void richTextBox1_TextChanged(object sender, EventArgs e)
        //{
           

        }

        private void button2_Click(object sender, EventArgs e)
        {
            char letra;
            string expresion=""; 

            Regex er = new Regex("^[0-9]+([.|,][0-9]+)?$");

            char[] arr = richTextBox1.Text.ToArray();

            for (int i = 0; i < arr.Length; i++)
            {
                letra = arr[i];
                expresion += letra; 

                if (er.IsMatch(expresion))
                {
                    richTextBox1.Select(i-(expresion.Length-1), expresion.Length);
                    richTextBox1.SelectionColor = Color.Red;
                }
                

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            TreeNode nodo = new TreeNode();
            if (nodo.Text!="")
            {
                richTextBox1.Text = "no esta vacio";
            }
            else
            {
                richTextBox1.Text = "esta vacio";
            }
        }
    }
}
